﻿using System.IO;
using TRLogicLLC.Core.AppPath.Root.Interfaces;

namespace NUnitTest.Core.Root
{
    public class TestAppRootPath : IAppRootPath
    {
        #region Имплемент

        public string GetRootPath => Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Test") + @"\";

        #endregion
    }
}