﻿using System.IO;
using System.Threading.Tasks;
using Commons.DAOProviders.Interfaces;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using NUnitTest.Core;
using TRLogicLLC.Core.AppPath.Directories.Interfaces;
using TRLogicLLC.Core.Upload;

namespace NUnitTest
{
    [TestFixture]
    public class TestUpload
    {
        #region Поля

        private IWebHost _webHost;

        #endregion

        #region Методы

        [SetUp]
        public void Setup()
        {
            _webHost = WebHost.CreateDefaultBuilder()
                .UseStartup<TestStartup>()
                .Build();
        }

        [Test]
        [Order(0)]
        public void UploadFile()
        {
            var appDirectories = _webHost.Services.GetService<IAppDirectories>();
            var providerJson = _webHost.Services.GetService<IProviderJson>();

            var files = new FormFileCollection();

            using (var stream = new MemoryStream(new byte[] {1, 2, 3, 4, 5, 6, 7, 8}))
            {
                var file = new FormFile(stream, 0, stream.Length, null, "TestFiles.jpg")
                {
                    Headers = new HeaderDictionary(),
                    ContentType = "application/jpg"
                };

                files.Add(file);

                var tasks = new UploadFacade().Upload(files, null, null,
                    appDirectories, providerJson);

                Task.WaitAll(tasks.ToArray());
            }

            Assert.IsTrue(File.Exists(Path.Combine(appDirectories.GetDirectoryImage, "TestFiles.jpg")));
        }

        [Test]
        [Order(1)]
        public void UploadJson()
        {
            var appDirectories = _webHost.Services.GetService<IAppDirectories>();
            var providerJson = _webHost.Services.GetService<IProviderJson>();

            var data = "{\"Name\":\"TestJson.jpg\",\"Data\":\"iVBORw0KGgoAAAANSUhEUgAAAMwAAACmCAIAAAAZL/eZAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAavSURBVHhe7dJdchxZDkNhL2QeZ / 876zXMOMJHCDXF2y6F2YVMJr7gi1BU / TDxIyIiIl73v4hpdEuII + bQLSGOmEO3hDhiDt0S4og5dEuII + bQLSGOmEO3hDhiDt0S4og5dEuII + bQLSGOmEO3hDhiDt0S4og5dEuII + bQLSGOmEO3hDhiDt0S4og5dEuII + bQLSGOmEO3hDhiDt0S4og5dEuII + bQLSGOmEO3hDhiDt0S4og5dEuII + bQLSGOmEO3hDhiDt0S4og5dEuII + bQLSGOmEO3hDhiDt0S4og5dEuI42V//ee/v4a/4wu6JcTxAtXr8/BafEK3hDjOSqtOw3akZN9SavTK8J/PRreEOP6uVOe7w7s8Fd0S4vhQ6tLO62vPRLeE+PFKP07D9ofy6mnYfgy6JcQPVgpxGrYPynI7rD4A3RLiRyolaIfV15T/bYfV1eiWED9MefDtsPp95X3aYXUpuiXEj1Eedjus/pnynqdhexe6JcTblUd7GrbnlPc/Ddtb0C0h3qs8znZY/TeVT2yH1fujW0K8UXmE7bD6LuXT22H1zuiWEO9SHls7rDqUb9IOq/dEt4R4hfKcTsO2W/lWp2H7VuiWEN9ceTDtsHo95Xu2w+pN0C0hvq3yMNph9drKd26H1cujW0J8Q+UBtMPqfZTv3w6rF0a3hPg+ysVPw/Y9ld9yGravh24J8R2UE7fD6hbl17XD6pXQLSG+tnLWdljdqPzSdli9BrolxFdVTtkOq9uVX90Oq250S4gvptzuNGw/SbnAadg2oVtCfBnlWO2w+mzlJu2w+nZ0S4gvoByoHVbjQ7lPO6y+Ed0SYrdyl6/DXnTKrb4Oe+9Ct4TYrRzl87ARv1Pu9nnYeBe6JcRu5Si/htfim8oZfw4vvAvdEmI371H28d6Tbgmxm/co+3jvSbeE2M17lH2896RbQuzmPco+3nvSLSF28x5lH+896ZYQu3mPso/3nnRLiN28R9nHe0+6JcRu3qPs470n3RJiN+9R9vHek24JsZv3KPt470m3hNjNe5R9vPekW0Ls5j3KPt570i0hdvMeZR/vPemWELt5j7KP9550S4jdvEfZx3tPuiXEbt6j7OO9J90SYjfvUfbx3pNuCbGb9yj7eO9Jt4TYzXuUfbz3pFtC7OY9yj7ee9ItIXbzHmUf7z3plhC7eY+yj/eedEuI3bxH2cd7T7olxG7eo+zjvSfdEmI371H28d6Tbgmxm/co+3jvSbeE2M17lH2896RbQuzmPco+3nvSLSF28x5lH+896ZYQu3mPso/3nnRLiN28R9nHe0+6JcRu3qPs470n3RJiN+9R9vHek24JsZv3KPt470m3hNjNe5R9vPekW0Ls5j3KPt570i0hdvMeZR/vPemWELt5j7KP9550S4jdvEfZx3tPuiXEbt6j7OO9J90SYjfvUfbx3pNuCbGb9yj7eO9Jt4TYzXuUfbz3pFtC7OY9yj7ee9ItIXbzHmUf7z3plhC7eY+yj/eedEuI3cpRfg4vxPeVS/4cXngXuiXEbuUon4eN+J1yt8/DxrvQLSF2K0f5OuxFp9zq67D3LnRLiC+g3KUdVuNDuU87rL4R3RLiyygHaofVZys3aYfVt6NbQnwx5VinYftJygVOw7YJ3RLiqyq3a4fV7cqvPg3bVnRLiK+t3LEdVjcqv7QdVq+BbgnxHZSztsPqFuXXtcPqldAtIb6PcuLTsH1b5ee0w+r10C0hvpty7tOwfR/l+5+G7auiW0J8W+X67bB6beU7t8Pq5dEtIb658jDaYfV6yvdsh9WboFtCvEJ5MO2weg3lu7XD6q3QLSFepDyk07DtUL7Jadi+IbolxBuVZ9YOq+9SPr0dVu+MbgnxXuURtsPqv6l8Yjus3h/dEuLtyuNsh9Vp5VPaYXULuiXEz1Ae7WnY/jPlPU/D9i50S4gfpjzpdlj9vvI+7bC6FN0S4kcqD74dVl9T/rcdVlejW0L8YKUE7bB6VvbbYfUB6JYQP14pxGnY/lBePQ3bj0G3hDg+lH608/raM9EtIY6/K3X57vAuT0W3hDg6pTqvDP/5bHRLiOOs1Og0bEdK9idKqzS8HB/olhDHy9Kt36JbQhwxh24JccQcuiXEEXPolhBHzKFbQhwxh24JccQcuiXEEXPolhBHzKFbQhwxh24JccQcuiXEEXPolhBHzKFbQhwxh24JccQcuiXEEXPolhBHzKFbQhwxh24JccQcuiXEEXPolhBHzKFbQhwxh24JccQcuiXEEXPoVkRERPyzHz/+D+djkmp1GwrSAAAAAElFTkSuQmCC\"}";

            var tasks = new UploadFacade().Upload(new FormFileCollection(), data, null,
                appDirectories, providerJson);

            Task.WaitAll(tasks.ToArray());

            Assert.IsTrue(File.Exists(Path.Combine(appDirectories.GetDirectoryImage, "TestJson.jpg")));
        }

        [Test]
        [Order(2)]
        public void UploadUrl()
        {
            var appDirectories = _webHost.Services.GetService<IAppDirectories>();
            var providerJson = _webHost.Services.GetService<IProviderJson>();

            var tasks = new UploadFacade().Upload(new FormFileCollection(), null, "https://f.vividscreen.info/soft/6cff880ea892b9fa4028a37a1bc9aa51/Funny-Cat-In-Round-Glasses-2880x1920.jpg",
                appDirectories, providerJson);

            Task.WaitAll(tasks.ToArray());

            Assert.IsTrue(File.Exists(Path.Combine(appDirectories.GetDirectoryImage, "Funny-Cat-In-Round-Glasses-2880x1920.jpg")));
        }

        #endregion
    }
}