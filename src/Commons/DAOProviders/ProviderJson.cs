﻿using System.Text;
using Commons.DAOProviders.Interfaces;
using Newtonsoft.Json;

namespace Commons.DAOProviders
{
    public class ProviderJson : IProviderJson
    {
        #region Поля

        public Encoding EncodingJson { get; }

        #endregion

        #region Конструкторы

        public ProviderJson()
        {
            EncodingJson = Encoding.UTF8;
        }

        public ProviderJson(Encoding encodingJson)
        {
            EncodingJson = encodingJson;
        }

        #endregion

        #region Методы

        public string SerializeData<T>(T obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        public byte[] SerializeDataByte<T>(T obj)
        {
            return EncodingJson.GetBytes(JsonConvert.SerializeObject(obj));
        }

        public T Deserialize<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        public T Deserialize<T>(byte[] json)
        {
            return JsonConvert.DeserializeObject<T>(EncodingJson.GetString(json));
        }

        #endregion
    }
}