﻿using System;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using Commons.DAOProviders.Interfaces;

namespace Commons.DAOProviders
{
    public class ProviderXml : IProviderXml
    {
        #region Методы

        /// <summary>
        /// Прочитать форматированные данные из файла.
        /// </summary>
        public T ReadSerializerData<T>(string patch) where T : class
        {
            try
            {
                if (!File.Exists(patch))
                    return null;

                var serializer = new XmlSerializer(typeof(T));

                using (var streamReader = new StreamReader(patch, Encoding.UTF8))
                {
                    var value = Encoding.Default.GetBytes(streamReader.ReadToEnd());

                    return (T) serializer.Deserialize(new MemoryStream(value));
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Записать форматированные данные в файл.
        /// </summary>
        public bool WriteSerializerData<T>(string patch, T data) where T : class
        {
            try
            {
                var serializer = new XmlSerializer(typeof(T));

                var memoryStream = new MemoryStream();

                serializer.Serialize(memoryStream, data);

                var resultSerializer = Encoding.Default.GetString(memoryStream.ToArray());

                using (var streamWriter = new StreamWriter(patch, false, Encoding.UTF8))
                {
                    streamWriter.Write(resultSerializer);
                    streamWriter.Flush();

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion
    }
}