﻿namespace Commons.DAOProviders.Interfaces
{
    public interface IProviderJson
    {
        string SerializeData<T>(T obj);
        byte[] SerializeDataByte<T>(T obj);
        T Deserialize<T>(string json);
        T Deserialize<T>(byte[] json);
    }
}