﻿namespace Commons.DAOProviders.Interfaces
{
    public interface IProviderXml
    {
        T ReadSerializerData<T>(string patch) where T : class;
        bool WriteSerializerData<T>(string patch, T data) where T : class;
    }
}