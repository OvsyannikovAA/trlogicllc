﻿using System;
using System.Text;
using Commons.DAOProviders;
using Commons.DAOProviders.Interfaces;
using Database.SqlServer.Core;
using Database.SqlServer.TRLogicDb;
using Database.SqlServer.TRLogicDb.Handlers;
using Database.SqlServer.TRLogicDb.Handlers.Interfaces;
using Database.SqlServer.TRLogicLog;
using Database.SqlServer.TRLogicLog.Handlers;
using Database.SqlServer.TRLogicLog.Handlers.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using TRLogicLLC.Core.AppPath.Directories;
using TRLogicLLC.Core.AppPath.Directories.Interfaces;
using TRLogicLLC.Core.AppPath.Files;
using TRLogicLLC.Core.AppPath.Root;
using TRLogicLLC.Core.DAO.XML;
using TRLogicLLC.Core.DAOProviders;
using TRLogicLLC.Core.DAOProviders.SettingsDefault;
using TRLogicLLC.Core.ServiceProxy;
using TRLogicLLC.Core.ServiceProxy.Interfaces;
using TRLogicLLC.Filters;

namespace TRLogicLLC
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            var settingsBase = new ProviderSettingsBase(new Settings<DSettingsBase>(
                new PathSettingsBase(new AppRootPath()),
                new SettingsBaseDefault(),
                new ProviderXml()));

            var settingsBaseLog = new ProviderSettingsBase(new Settings<DSettingsBase>(
                new PathSettingsBaseLog(new AppRootPath()),
                new SettingsBaseLogDefault(),
                new ProviderXml()));

            var optionsBase = new SqlServerConnectionOptions<TRLogicDbContext>(
                settingsBase.Settings.Data.DataSource,
                settingsBase.Settings.Data.Schema,
                settingsBase.Settings.Data.User,
                settingsBase.Settings.Data.Password);

            var optionsBaseLog = new SqlServerConnectionOptions<TRLogicLogContext>(
                settingsBaseLog.Settings.Data.DataSource,
                settingsBaseLog.Settings.Data.Schema,
                settingsBaseLog.Settings.Data.User,
                settingsBaseLog.Settings.Data.Password);

            services.AddSingleton(settingsBase);
            services.AddSingleton(settingsBaseLog);

            services.AddSingleton(optionsBase);
            services.AddSingleton(optionsBaseLog);
            
            services.AddSingleton<IProviderJson>(new ProviderJson(Encoding.UTF8));
            services.AddSingleton<IAppDirectories>(new AppDirectories(new AppRootPath()));
            
            services.AddScoped(_ => new TRLogicDbContext(optionsBase.Options));
            services.AddScoped(_ => new TRLogicLogContext(optionsBaseLog.Options));
            
            services.AddTransient<ISaveExceptionDetail, SaveExceptionDetail>();
            services.AddTransient<IDbSaveImage, SaveImage>();

            services.AddMvc(options => { options.Filters.Add(new ExceptionLoggerAttribute()); });

            services.AddSingleton<IServiceProxy, ServiceProxy>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider)
        {
            app.UseStaticFiles();

            var appCreateDirectories = new AppCreateDirectories(new AppDirectories(new AppRootPath()));
            appCreateDirectories.CreateDirectoryImage();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}");
            });

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                    appCreateDirectories.AppDirectories.GetDirectoryImage),
                RequestPath = AppDirectories.WebDirectoryNameImage
            });

            app.UseDirectoryBrowser(new DirectoryBrowserOptions
            {
                FileProvider = new PhysicalFileProvider(
                    appCreateDirectories.AppDirectories.GetDirectoryImage),
                RequestPath = AppDirectories.WebDirectoryNameImage
            });

            AppServiceProxy.Initialize(serviceProvider.GetService<IServiceProxy>());
        }
    }
}