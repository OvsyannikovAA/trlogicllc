﻿using System;
using System.Threading.Tasks;
using Database.SqlServer.Core;
using Database.SqlServer.TRLogicLog;
using Database.SqlServer.TRLogicLog.Handlers.Interfaces;
using Database.SqlServer.TRLogicLog.Model;
using Microsoft.AspNetCore.Mvc.Filters;
using TRLogicLLC.Core.ServiceProxy;

namespace TRLogicLLC.Filters
{
    public class ExceptionLoggerAttribute : Attribute, IAsyncExceptionFilter
    {
        public async Task OnExceptionAsync(ExceptionContext context)
        {
            var saveExceptionDetail = AppServiceProxy.Service.GetService<ISaveExceptionDetail>();
            var options = AppServiceProxy.Service.GetService<SqlServerConnectionOptions<TRLogicLogContext>>();

            using (var contextLog = new TRLogicLogContext(options.Options))
            {
                var exceptionDetail = new ExceptionDetail
                {
                    ExceptionMessage = context.Exception.Message,
                    StackTrace = context.Exception.StackTrace,
                    ControllerName = context.RouteData.Values["controller"].ToString(),
                    ActionName = context.RouteData.Values["action"].ToString(),
                    Date = DateTime.UtcNow
                };

                await saveExceptionDetail.Save(contextLog, exceptionDetail);
            }
            

            context.ExceptionHandled = true;
        }
    }
}