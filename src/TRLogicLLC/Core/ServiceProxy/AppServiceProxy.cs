﻿using TRLogicLLC.Core.ServiceProxy.Interfaces;

namespace TRLogicLLC.Core.ServiceProxy
{
    public static class AppServiceProxy
    {
        public static void Initialize(IServiceProxy proxy)
        {
            Service = proxy;
        }

        public static IServiceProxy Service;
    }
}