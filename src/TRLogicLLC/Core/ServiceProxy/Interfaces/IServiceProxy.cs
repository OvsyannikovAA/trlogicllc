﻿using System;
using System.Collections.Generic;

namespace TRLogicLLC.Core.ServiceProxy.Interfaces
{
    public interface IServiceProxy
    {
        T GetService<T>();
        IEnumerable<T> GetServices<T>();
        object GetService(Type type);
        IEnumerable<object> GetServices(Type type);
    }
}