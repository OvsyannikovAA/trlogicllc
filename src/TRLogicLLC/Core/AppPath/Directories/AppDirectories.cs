﻿using TRLogicLLC.Core.AppPath.Directories.Interfaces;
using TRLogicLLC.Core.AppPath.Root.Interfaces;

namespace TRLogicLLC.Core.AppPath.Directories
{
    public class AppDirectories : IAppDirectories
    {
        #region Поля

        private readonly IAppRootPath _appRootPath;

        public const string DirectoryNameFiles = @"Files\";
        public const string DirectoryNameImage = DirectoryNameFiles + @"Image\";

        public const string WebDirectoryNameFiles = "/Files";
        public const string WebDirectoryNameImage = WebDirectoryNameFiles + "/Image";

        #endregion

        #region Конструкторы

        public AppDirectories(IAppRootPath appRootPath)
        {
            _appRootPath = appRootPath;
        }

        #endregion

        #region Свойства
        
        public string GetDirectoryImage => _appRootPath.GetRootPath + DirectoryNameImage;
        public string GetWebDirectoryImage => WebDirectoryNameImage + "/";

        #endregion
    }
}