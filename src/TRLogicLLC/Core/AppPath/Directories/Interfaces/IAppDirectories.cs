﻿namespace TRLogicLLC.Core.AppPath.Directories.Interfaces
{
    public interface IAppDirectories
    {
        string GetDirectoryImage { get; }
        string GetWebDirectoryImage { get; }
    }
}