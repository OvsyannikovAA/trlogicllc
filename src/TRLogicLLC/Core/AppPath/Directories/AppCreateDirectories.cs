﻿using System.IO;
using TRLogicLLC.Core.AppPath.Directories.Interfaces;

namespace TRLogicLLC.Core.AppPath.Directories
{
    public class AppCreateDirectories
    {
        #region Поля

        public readonly IAppDirectories AppDirectories;

        #endregion

        #region Конструкторы

        public AppCreateDirectories(IAppDirectories appDirectories)
        {
            AppDirectories = appDirectories;
        }

        #endregion

        #region Методы

        public void CreateDirectoryImage()
        {
            if (!Directory.Exists(AppDirectories.GetDirectoryImage))
                Directory.CreateDirectory(AppDirectories.GetDirectoryImage);
        }

        #endregion
    }
}