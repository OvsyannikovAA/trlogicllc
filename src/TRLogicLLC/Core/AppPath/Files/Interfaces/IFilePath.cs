﻿namespace TRLogicLLC.Core.AppPath.Files.Interfaces
{
    public interface IFilePath
    {
        string Path { get; }
    }
}