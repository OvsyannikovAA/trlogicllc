﻿using TRLogicLLC.Core.AppPath.Files.Interfaces;
using TRLogicLLC.Core.AppPath.Root.Interfaces;

namespace TRLogicLLC.Core.AppPath.Files
{
    public class PathSettingsBase : IFilePath
    {
        #region Поля

        private readonly IAppRootPath _appRootPath;

        public const string FileNameSettingsBase = "SettingsBase.xml";

        #endregion

        #region Конструкторы

        public PathSettingsBase(IAppRootPath appRootPath)
        {
            _appRootPath = appRootPath;
        }

        #endregion

        #region Свойства

        public string Path => _appRootPath.GetRootPath + FileNameSettingsBase;

        #endregion
    }
}