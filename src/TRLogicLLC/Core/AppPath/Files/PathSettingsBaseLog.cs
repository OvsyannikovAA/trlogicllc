﻿using TRLogicLLC.Core.AppPath.Files.Interfaces;
using TRLogicLLC.Core.AppPath.Root.Interfaces;

namespace TRLogicLLC.Core.AppPath.Files
{
    public class PathSettingsBaseLog : IFilePath
    {
        #region Поля

        private readonly IAppRootPath _appRootPath;
        
        public const string FileNameSettingsBaseLog = "SettingsBaseLog.xml";

        #endregion

        #region Конструкторы

        public PathSettingsBaseLog(IAppRootPath appRootPath)
        {
            _appRootPath = appRootPath;
        }

        #endregion

        #region Свойства

        public string Path => _appRootPath.GetRootPath + FileNameSettingsBaseLog;

        #endregion
    }
}