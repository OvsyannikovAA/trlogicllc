﻿namespace TRLogicLLC.Core.AppPath.Root.Interfaces
{
    public interface IAppRootPath
    {
        string GetRootPath { get; }
    }
}