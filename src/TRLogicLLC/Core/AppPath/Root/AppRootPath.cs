﻿using System.IO;
using TRLogicLLC.Core.AppPath.Root.Interfaces;

namespace TRLogicLLC.Core.AppPath.Root
{
    public class AppRootPath : IAppRootPath
    {
        #region Имплемент

        public string GetRootPath => Path.Combine(Directory.GetCurrentDirectory(), "wwwroot") + @"\";

        #endregion
    }
}