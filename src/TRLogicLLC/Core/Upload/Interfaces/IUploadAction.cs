﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TRLogicLLC.Core.Upload.Interfaces
{
    public interface IUploadAction
    {
        #region Методы

        Task<List<Tuple<string, string>>> Handler();

        #endregion
    }
}