﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Commons.DAOProviders.Interfaces;
using Microsoft.AspNetCore.Http;
using TRLogicLLC.Core.AppPath.Directories.Interfaces;

namespace TRLogicLLC.Core.Upload
{
    public class UploadFacade
    {
        #region Методы

        public List<Task<List<Tuple<string, string>>>> Upload(IFormFileCollection files, string json, string url,
            IAppDirectories appDirectories,
            IProviderJson providerJson)
        {
            var tasks = new List<Task<List<Tuple<string, string>>>>();

            if (files.Count > 0)
            {
                tasks.Add(new UploadFiles(files, appDirectories).Handler());
            }

            if (!string.IsNullOrEmpty(json))
            {
                tasks.Add(new UploadJson(json, appDirectories, providerJson).Handler());
            }

            if (!string.IsNullOrEmpty(url))
            {
                tasks.Add(new UploadUrl(url, appDirectories).Handler());
            }

            return tasks;
        }

        #endregion
    }
}