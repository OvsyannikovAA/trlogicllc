﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Commons.DAOProviders.Interfaces;
using TRLogicLLC.Core.AppPath.Directories.Interfaces;
using TRLogicLLC.Core.Upload.Interfaces;
using TRLogicLLC.Models;

namespace TRLogicLLC.Core.Upload
{
    public class UploadJson : IUploadAction
    {
        #region Поля

        private readonly string _json;
        private readonly IAppDirectories _appDirectories;
        private readonly IProviderJson _providerJson;

        #endregion

        #region Конструкторы

        public UploadJson(string json, IAppDirectories appDirectories, IProviderJson providerJson)
        {
            _json = json;
            _appDirectories = appDirectories;
            _providerJson = providerJson;
        }

        #endregion

        #region Методы

        public async Task<List<Tuple<string, string>>> Handler()
        {
            var result = new List<Tuple<string, string>>();

            try
            {
                var model = _providerJson.Deserialize<FileJson>(_json);

                var path = _appDirectories.GetDirectoryImage + model.Name;
                var pathWeb = _appDirectories.GetWebDirectoryImage + model.Name;

                using (var fileStream = new FileStream(path, FileMode.Create))
                {
                    var data = Convert.FromBase64String(model.Data);

                    await fileStream.WriteAsync(data, 0, data.Length);

                    result.Add(new Tuple<string, string>(path, pathWeb));
                }
            }
            catch (Exception)
            {
                // ignored
            }

            return result;
        }

        #endregion
    }
}