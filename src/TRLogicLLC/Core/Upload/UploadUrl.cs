﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using TRLogicLLC.Core.AppPath.Directories.Interfaces;
using TRLogicLLC.Core.Upload.Interfaces;

namespace TRLogicLLC.Core.Upload
{
    public class UploadUrl : IUploadAction
    {
        #region Поля

        private readonly string _url;
        private readonly IAppDirectories _appDirectories;

        #endregion

        #region Конструкторы

        public UploadUrl(string url, IAppDirectories appDirectories)
        {
            _url = url;
            _appDirectories = appDirectories;
        }

        #endregion

        #region Методы

        public async Task<List<Tuple<string, string>>> Handler()
        {
            var result = new List<Tuple<string, string>>();

            try
            {
                var uri = new Uri(_url);
                using (var client = new WebClient())
                {
                    var path = _appDirectories.GetDirectoryImage + Path.GetFileName(uri.LocalPath);
                    var pathWeb = _appDirectories.GetWebDirectoryImage + Path.GetFileName(uri.LocalPath);

                    await client.DownloadFileTaskAsync(uri, path);

                    result.Add(new Tuple<string, string>(path, pathWeb));
                }
            }
            catch (Exception)
            {
                // ignored
            }

            return result;
        }

        #endregion
    }
}