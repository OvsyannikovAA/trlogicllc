﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using TRLogicLLC.Core.AppPath.Directories.Interfaces;
using TRLogicLLC.Core.Upload.Interfaces;

namespace TRLogicLLC.Core.Upload
{
    public class UploadFiles : IUploadAction
    {
        #region Поля

        private readonly IFormFileCollection _files;
        private readonly IAppDirectories _appDirectories;

        #endregion

        #region Конструкторы

        public UploadFiles(IFormFileCollection files, IAppDirectories appDirectories)
        {
            _files = files;
            _appDirectories = appDirectories;
        }

        #endregion

        #region Методы

        public async Task<List<Tuple<string, string>>> Handler()
        {
            var result = new List<Tuple<string, string>>();

            try
            {
                foreach (var uploadedFile in _files)
                {
                    if (uploadedFile.Length > 0)
                    {
                        var path = _appDirectories.GetDirectoryImage + uploadedFile.FileName;
                        var pathWeb = _appDirectories.GetWebDirectoryImage + uploadedFile.FileName;

                        using (var fileStream = new FileStream(path, FileMode.Create))
                        {
                            await uploadedFile.CopyToAsync(fileStream);

                            result.Add(new Tuple<string, string>(path, pathWeb));
                        }
                    }
                }
            }
            catch (Exception)
            {
                // ignored
            }

            return result;
        }

        #endregion
    }
}