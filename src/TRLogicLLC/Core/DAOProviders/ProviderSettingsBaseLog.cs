﻿using TRLogicLLC.Core.DAO.Interfaces;
using TRLogicLLC.Core.DAOProviders.Interfaces;

namespace TRLogicLLC.Core.DAOProviders
{
    public class ProviderSettingsBaseLog
    {
        #region Поля

        public readonly IProvider<ISettingsBase> Settings;

        #endregion

        #region Конструкторы

        public ProviderSettingsBaseLog(IProvider<ISettingsBase> settings)
        {
            Settings = settings;
        }

        #endregion
    }
}