﻿using TRLogicLLC.Core.DAO.XML;
using TRLogicLLC.Core.DAOProviders.Interfaces;

namespace TRLogicLLC.Core.DAOProviders.SettingsDefault
{
    public class SettingsBaseLogDefault : ISettingsDefault<DSettingsBase>
    {
        public DSettingsBase Default
        {
            get
            {
                var value = new DSettingsBase
                {
                    DataSource = "DESKTOP-DJG9M0D\\SQLEXPRESS",
                    Schema = "TRLogicLog",
                    User = "admin",
                    Password = "admin"
                };

                return value;
            }
        }
    }
}