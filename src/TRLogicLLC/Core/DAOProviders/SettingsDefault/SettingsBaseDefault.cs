﻿using TRLogicLLC.Core.DAO.XML;
using TRLogicLLC.Core.DAOProviders.Interfaces;

namespace TRLogicLLC.Core.DAOProviders.SettingsDefault
{
    public class SettingsBaseDefault : ISettingsDefault<DSettingsBase>
    {
        public DSettingsBase Default
        {
            get
            {
                var result = new DSettingsBase
                {
                    DataSource = "DESKTOP-DJG9M0D\\SQLEXPRESS",
                    Schema = "TRLogicDb",
                    User = "admin",
                    Password = "admin"
                };

                return result;
            }
        }
    }
}