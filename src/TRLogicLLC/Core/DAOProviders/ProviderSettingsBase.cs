﻿using TRLogicLLC.Core.DAO.Interfaces;
using TRLogicLLC.Core.DAOProviders.Interfaces;

namespace TRLogicLLC.Core.DAOProviders
{
    public class ProviderSettingsBase
    {
        #region Поля

        public readonly IProvider<ISettingsBase> Settings;

        #endregion

        #region Конструкторы

        public ProviderSettingsBase(IProvider<ISettingsBase> settings)
        {
            Settings = settings;
        }

        #endregion
    }
}