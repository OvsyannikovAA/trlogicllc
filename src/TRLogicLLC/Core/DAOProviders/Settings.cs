﻿using Commons.DAOProviders.Interfaces;
using TRLogicLLC.Core.AppPath.Files.Interfaces;
using TRLogicLLC.Core.DAOProviders.Interfaces;

namespace TRLogicLLC.Core.DAOProviders
{
    public class Settings<T> : IProvider<T> where T : class
    {
        #region Поля

        private readonly IFilePath _path;
        private readonly ISettingsDefault<T> _default;
        private readonly IProviderXml _providerXml;

        #endregion

        #region Конструкторы

        public Settings(IFilePath path, ISettingsDefault<T> @default,
            IProviderXml providerXml)
        {
            _path = path;
            _default = @default;
            _providerXml = providerXml;

            Create();
        }

        #endregion

        #region Методы

        public void Create()
        {
            Data = _providerXml.ReadSerializerData<T>(_path.Path);

            if (Data != null) return;

            Data = _default.Default;
            Save();
        }

        public void Save()
        {
            if (Data != null)
                _providerXml.WriteSerializerData(_path.Path, Data);
        }

        #endregion

        #region Свойства

        public T Data { get; private set; }

        #endregion
    }
}