﻿namespace TRLogicLLC.Core.DAOProviders.Interfaces
{
    public interface ISettingsDefault<out T>
    {
        T Default { get; }
    }
}