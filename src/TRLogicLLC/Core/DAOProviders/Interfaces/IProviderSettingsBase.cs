﻿namespace TRLogicLLC.Core.DAOProviders.Interfaces
{
    public interface IProvider<out T>
    {
        void Create();
        void Save();

        T Data { get; }
    }
}