﻿using System;
using TRLogicLLC.Core.DAO.Interfaces;

namespace TRLogicLLC.Core.DAO.XML
{
    [Serializable]
    public class DSettingsBase : ISettingsBase
    {
        #region Свойства

        public string DataSource { get; set; }
        public string Schema { get; set; }
        public string User { get; set; }
        public string Password { get; set; }

        #endregion
    }
}