﻿namespace TRLogicLLC.Core.DAO.Interfaces
{
    public interface ISettingsBase
    {
        string DataSource { get; set; }
        string Schema { get; set; }
        string User { get; set; }
        string Password { get; set; }
    }
}