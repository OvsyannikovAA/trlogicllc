﻿namespace TRLogicLLC.Models
{
    public class FileJson
    {
        /// <summary>
        /// Имя файла с расширением.
        /// </summary>
        public string Name;

        /// <summary>
        /// Тело файла в формате BASE64.
        /// </summary>
        public string Data;
    }
}