﻿using System.Linq;
using Database.SqlServer.TRLogicDb;
using Microsoft.AspNetCore.Mvc;

namespace TRLogicLLC.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index([FromServices] TRLogicDbContext context)
        {
            ViewBag.Images = context.Images.Where(c => c.Id < 100).ToList();

            return View();
        }
    }
}