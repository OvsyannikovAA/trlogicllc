﻿using System;
using System.Threading.Tasks;
using Commons.DAOProviders.Interfaces;
using Database.SqlServer.Core;
using Database.SqlServer.TRLogicDb;
using Database.SqlServer.TRLogicDb.Handlers.Interfaces;
using Database.SqlServer.TRLogicDb.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TRLogicLLC.Core.AppPath.Directories.Interfaces;
using TRLogicLLC.Core.Upload;

namespace TRLogicLLC.Controllers
{
    [Route("api/files/upload")]
    public class UploadController : Controller
    {
        private readonly IDbSaveImage _saveImage;
        private readonly SqlServerConnectionOptions<TRLogicDbContext> _options;

        public UploadController(IDbSaveImage saveImage, SqlServerConnectionOptions<TRLogicDbContext> options)
        {
            _saveImage = saveImage;
            _options = options;
        }

        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult UploadFile(IFormFileCollection files, string json, string url,
            [FromServices] IAppDirectories appDirectories,
            [FromServices] IProviderJson providerJson)
        {
            var tasks = new UploadFacade().Upload(files, json, url, appDirectories, providerJson);
            
            Task.Factory.ContinueWhenAll(tasks.ToArray(), async task =>
            {
                using (var context = new TRLogicDbContext(_options.Options))
                {
                    foreach (var value in task)
                    {
                        if (value.Result != null)
                        {
                            foreach (var tuple in value.Result)
                            {
                                await _saveImage.Save(context, new Image
                                {
                                    DateRegistration = DateTime.Now,
                                    DateLastUpdate = DateTime.Now,
                                    Path = tuple.Item2
                                });
                            }
                        }
                    }

                    await context.SaveChangesAsync();
                }
            });

            return RedirectToAction("Index", "Home");
        }
    }
}