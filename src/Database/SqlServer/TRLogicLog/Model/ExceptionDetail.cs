﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Database.SqlServer.TRLogicLog.Model
{
    [Table("Exceptions")]
    public class ExceptionDetail
    {
        #region Свойства

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        /// <summary>
        /// Сообщение об исключении.
        /// </summary>
        public string ExceptionMessage { get; set; }

        /// <summary>
        /// Контроллер, где возникло исключение.
        /// </summary>
        public string ControllerName { get; set; }

        /// <summary>
        /// Действие, где возникло исключение.
        /// </summary>
        public string ActionName { get; set; }

        /// <summary>
        /// Стек исключения.
        /// </summary>
        public string StackTrace { get; set; }

        /// <summary>
        /// Дата и время исключения.
        /// </summary>
        public DateTime Date { get; set; }

        #endregion
    }
}