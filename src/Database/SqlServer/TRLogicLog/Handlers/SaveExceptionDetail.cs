﻿using System.Threading.Tasks;
using Database.SqlServer.TRLogicLog.Handlers.Interfaces;
using Database.SqlServer.TRLogicLog.Model;

namespace Database.SqlServer.TRLogicLog.Handlers
{
    public class SaveExceptionDetail : ISaveExceptionDetail
    {
        #region Методы

        public async Task<int> Save(TRLogicLogContext context, ExceptionDetail detail)
        {
            context.ExceptionDetails.Add(detail);
            return await context.SaveChangesAsync();
        }

        #endregion
    }
}