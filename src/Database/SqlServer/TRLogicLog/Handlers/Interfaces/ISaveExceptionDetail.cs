﻿using System.Threading.Tasks;
using Database.SqlServer.TRLogicLog.Model;

namespace Database.SqlServer.TRLogicLog.Handlers.Interfaces
{
    public interface ISaveExceptionDetail
    {
        Task<int> Save(TRLogicLogContext context, ExceptionDetail detail);
    }
}