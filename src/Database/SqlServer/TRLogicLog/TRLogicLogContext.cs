﻿using Database.SqlServer.TRLogicLog.Model;
using Microsoft.EntityFrameworkCore;

namespace Database.SqlServer.TRLogicLog
{
    public sealed class TRLogicLogContext : DbContext
    {
        #region Конструкторы

        public TRLogicLogContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }

        #endregion

        #region Свойства

        public DbSet<ExceptionDetail> ExceptionDetails { get; set; }

        #endregion
    }
}