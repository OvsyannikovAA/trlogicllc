﻿using System.Linq;
using System.Threading.Tasks;
using Database.SqlServer.TRLogicDb.Handlers.Interfaces;
using Database.SqlServer.TRLogicDb.Model;

namespace Database.SqlServer.TRLogicDb.Handlers
{
    public class SaveImage : IDbSaveImage
    {
        #region Методы

        public async Task<int?> Save(TRLogicDbContext context, Image value)
        {
            var findImage = context.Images.FirstOrDefault(i => i.Path.Equals(value.Path));

            if (findImage == null)
            {
                context.Images.Add(value);
                return await context.SaveChangesAsync();
            }

            return null;
        }

        #endregion
    }
}