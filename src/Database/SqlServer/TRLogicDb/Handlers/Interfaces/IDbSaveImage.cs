﻿using System.Threading.Tasks;
using Database.SqlServer.TRLogicDb.Model;

namespace Database.SqlServer.TRLogicDb.Handlers.Interfaces
{
    public interface IDbSaveImage
    {
        Task<int?> Save(TRLogicDbContext context, Image value);
    }
}