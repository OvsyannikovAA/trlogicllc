﻿using Database.SqlServer.TRLogicDb.Model;
using Microsoft.EntityFrameworkCore;

namespace Database.SqlServer.TRLogicDb
{
    public sealed class TRLogicDbContext : DbContext
    {
        #region Конструкторы

        public TRLogicDbContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }

        #endregion

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }

        #region Свойства

        public DbSet<Image> Images { get; set; }

        #endregion
    }
}