﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Database.SqlServer.TRLogicDb.Model
{
    public class Image
    {
        #region Поля

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        /// <summary>
        /// Дата создания изображения на сервере.
        /// </summary>
        public DateTime DateRegistration { get; set; }
        /// <summary>
        /// Дата последней модификации изображения.
        /// </summary>
        public DateTime DateLastUpdate { get; set; }

        /// <summary>
        /// Краткое описание изображения.
        /// </summary>
        [StringLength(64)]
        public string Note { get; set; }

        /// <summary>
        /// Путь к файлу изображения.
        /// </summary>
        [StringLength(256)]
        public string Path { get; set; }

        #endregion
    }
}