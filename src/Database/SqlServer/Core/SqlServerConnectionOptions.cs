﻿using Microsoft.EntityFrameworkCore;

namespace Database.SqlServer.Core
{
    public class SqlServerConnectionOptions<T> where T : DbContext
    {
        #region Поля

        public readonly string ConnStr;

        #endregion

        #region Конструкторы

        public SqlServerConnectionOptions(string dataSource, string schema, string user, string password)
        {
            ConnStr =
                $"data source={dataSource};" +
                $"initial catalog={schema};" +
                $"user id={user};" +
                $"password={password};" +
                "persist security info=True;" +
                "MultipleActiveResultSets=True;" +
                "Trusted_Connection=True;";
        }

        #endregion

        #region Свойства

        public DbContextOptions<T> Options
        {
            get
            {
                var optionsBuilder = new DbContextOptionsBuilder<T>();

                return optionsBuilder.UseSqlServer(ConnStr).Options;
            }
        }

        #endregion
    }
}